libshadertoy (0.1.8) unstable; urgency=medium

  * Add support for defining uniforms at run-time. See example15-uniforms
  * Fix buffer timing measuring extra geometry binding

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Wed, 21 Feb 2018 16:15:05 +0100

libshadertoy (0.1.7) unstable; urgency=medium

  * Add default parameter for buffer query from context

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Thu, 08 Feb 2018 13:15:00 +0200

libshadertoy (0.1.6) unstable; urgency=medium

  * Add fragment shader execution time query support (iTimeDelta)
  * Fix unsupported inputs in examples
  * Fix mipmap generation
  * Fix various bugs when building from source
  * Fix examples deleting GL context before Shadertoy context

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Thu, 08 Feb 2018 12:30:00 +0200

libshadertoy (0.1.5) unstable; urgency=medium

  * Update dependencies for Xenial and Stretch

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Wed, 07 Feb 2018 15:00:00 +0200

libshadertoy (0.1.4) unstable; urgency=medium

  * Add support for Ubuntu Trusty (14.04 LTS)

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Tue, 06 Feb 2018 02:05:00 +0200

libshadertoy (0.1.3) unstable; urgency=medium

  * Remove API version from .so basename, include SOVERSION definition

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Fri, 31 Nov 2017 13:45:00 +0200

libshadertoy (0.1.2) unstable; urgency=medium

  * Fix package build including the wrong version of libshadertoy.so

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Thu, 30 Nov 2017 18:00:00 +0200

libshadertoy (0.1.1) unstable; urgency=medium

  * Fix compatibility issues
  * Add missing documentation

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Thu, 30 Nov 2017 13:45:00 +0200

libshadertoy (0.1.0) unstable; urgency=medium

  * Use GLM for vector arithmetic
  * Remove dependency on oglplus and dependent packages, use raw OpenGL instead
  * Remove serialization code and associated Boost dependency

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Wed, 29 Nov 2017 17:05:00 +0200

libshadertoy (0.0.10) unstable; urgency=medium

  * Fix CMakeLists.txt include order

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Fri, 23 Oct 2017 15:00:00 +0200

libshadertoy (0.0.9) unstable; urgency=medium

  * Various bug fixes

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Fri, 23 Oct 2017 12:35:00 +0200

libshadertoy (0.0.8) unstable; urgency=medium

  * Fix ToyBuffers copying configuration instead of referencing
  * Fix global header not including the TextureEngine header
  * Fix buildpackage script

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Fri, 23 Oct 2017 10:55:00 +0200

libshadertoy (0.0.7) unstable; urgency=medium

  * Fix iResolution not being set on Initialize

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Fri, 13 Oct 2017 15:52:00 +0200

libshadertoy (0.0.6) unstable; urgency=medium

  * Fix iResolution not being updated on resolution change through Allocate

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Fri, 13 Oct 2017 14:05:00 +0200

libshadertoy (0.0.5) unstable; urgency=medium

  * Fix support for iGlobalTime as well as iTime

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Thu, 12 Oct 2017 15:00:00 +0200

libshadertoy (0.0.4) unstable; urgency=medium

  * Fix example 50-shadertoy

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Wed, 04 Oct 2017 17:43:00 +0200

libshadertoy (0.0.3) unstable; urgency=medium

  * Add dump program binary context options

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Wed, 04 Oct 2017 14:05:00 +0200

libshadertoy (0.0.2) unstable; urgency=medium

  * Add buffer resizing support
  * Add ShaderToy API example
  * Add libjpeg support for loading progressive JPEGs
  * Fix: TextureEngine does not have any handlers registered by default

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Thu, 20 Jul 2017 11:33:20 +0200

libshadertoy (0.0.1) unstable; urgency=medium

  * Initial Release.

 -- Vincent Tavernier <vincent.tavernier@inria.fr>  Tue, 11 Jul 2017 15:05:18 +0200
