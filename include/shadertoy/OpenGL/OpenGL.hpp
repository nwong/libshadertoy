#ifndef _SHADERTOY_OPENGL_OPENGL_HPP_
#define _SHADERTOY_OPENGL_OPENGL_HPP_

#include "shadertoy/OpenGL/Caller.hpp"

#include "shadertoy/OpenGL/Resource.hpp"

#include "shadertoy/OpenGL/Buffer.hpp"
#include "shadertoy/OpenGL/Framebuffer.hpp"
#include "shadertoy/OpenGL/Program.hpp"
#include "shadertoy/OpenGL/Query.hpp"
#include "shadertoy/OpenGL/Renderbuffer.hpp"
#include "shadertoy/OpenGL/Shader.hpp"
#include "shadertoy/OpenGL/Texture.hpp"

#endif
